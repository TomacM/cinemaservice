package bobcat.cinema.repository;

import bobcat.cinema.model.ProjectionType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectionTypeRepositroy extends JpaRepository<ProjectionType, Long> {
}