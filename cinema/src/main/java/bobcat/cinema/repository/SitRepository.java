package bobcat.cinema.repository;

import bobcat.cinema.model.Sit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SitRepository extends JpaRepository<Sit, Long> {
}