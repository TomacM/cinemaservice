package bobcat.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Person director;

    @ManyToMany
    private Set<Person> actors = new HashSet<Person>();

    @ManyToMany
    private Set<Genre> genres = new HashSet<Genre>();

    @Column(nullable = false)
    private String duration;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Distributor distributor;

    @Column(nullable = false)
    private String country;

    @Column(nullable = false)
    private String year;

    @Column(nullable = false)
    private String description;
}