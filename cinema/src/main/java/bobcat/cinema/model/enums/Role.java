package bobcat.cinema.model.enums;

public enum Role {
    USER, ADMINISTRATOR
}