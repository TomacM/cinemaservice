package bobcat.cinema.service;

import bobcat.cinema.model.Movie;
import bobcat.cinema.model.Person;

import java.util.List;

public interface PersonServiceInterface {

    List<Person> findAll();

    Person findOne(long id);

    Person save(Person person);

    void remove(long id);
}