package bobcat.cinema.service;

import bobcat.cinema.dto.DistributorDTO;
import bobcat.cinema.model.Distributor;

import java.util.List;

public interface DistributorServiceDTOInterface {

    List<DistributorDTO> getAllDTOs(List<Distributor> distributor);

    DistributorDTO getDistributorDTO(Distributor dist);
}