package bobcat.cinema.service;

import bobcat.cinema.dto.PersonDTO;
import bobcat.cinema.model.Person;
import bobcat.cinema.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService implements PersonServiceInterface,PersonServiceDTOInterface{
    PersonRepository pr;

    @Override
    public List<PersonDTO> getAllDTOs(List<Person> persons) {
        List<PersonDTO> personDTOS = new ArrayList<>();
        for (Person person : persons) {
            PersonDTO dto = new PersonDTO(person);
            personDTOS.add(dto);
        }
        return personDTOS;
    }

    @Override
    public PersonDTO getPeronDto(Person person) {
        return new PersonDTO(person);
    }

    @Override
    public List<Person> findAll() {
        return pr.findAll();
    }

    @Override
    public Person findOne(long id) {
        return pr.findById(id).orElse(null);
    }

    @Override
    public Person save(Person person) {
        return pr.save(person);
    }

    @Override
    public void remove(long id) {
        pr.deleteById(id);
    }
}