package bobcat.cinema.service;

import bobcat.cinema.model.Genre;
import bobcat.cinema.model.Movie;

import java.util.List;

public interface GenreServiceInterface {

    List<Genre> findAll();

    Genre findOne(long id);

    Genre save(Genre genre);

    void remove(long id);
}
