package bobcat.cinema.service;

import bobcat.cinema.dto.DistributorDTO;
import bobcat.cinema.model.Distributor;
import bobcat.cinema.repository.DistributorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DistributorService implements DistributorServiceDTOInterface, DistributorServiceInterface {

    @Autowired
    DistributorRepository dr;


    @Override
    public List<DistributorDTO> getAllDTOs(List<Distributor> distributors) {
        List<DistributorDTO> dtos = new ArrayList<>();
        for (Distributor d : distributors) {
            DistributorDTO dto = new DistributorDTO(d);
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public DistributorDTO getDistributorDTO(Distributor dist) {
        DistributorDTO dto = new DistributorDTO(dist);
        return dto;
    }

    @Override
    public List<Distributor> findAll() {
        return dr.findAll();
    }

    @Override
    public Distributor findOne(long id) {
        return dr.findById(id).orElse(null);
    }

    @Override
    public Distributor save(Distributor dist) {
        return dr.save(dist);
    }

    @Override
    public void remove(long id) {
        dr.deleteById(id);
    }
}