package bobcat.cinema.service;

import bobcat.cinema.dto.MovieDTO;
import bobcat.cinema.model.Movie;

import java.util.List;

public interface MovieServiceDTOInterface {

    List<MovieDTO> getAllDTOs(List<Movie> movie);

    MovieDTO getMovieDto(Movie movie);
}