package bobcat.cinema.service;

import bobcat.cinema.dto.MovieDTO;
import bobcat.cinema.model.Movie;
import bobcat.cinema.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieService implements MovieServiceInterface, MovieServiceDTOInterface {

    @Autowired
    MovieRepository mr;

    @Override
    public List<MovieDTO> getAllDTOs(List<Movie> movies) {
        List<MovieDTO> movieDTOS = new ArrayList<>();
        for (Movie movie : movies) {
            MovieDTO dto = new MovieDTO(movie);
            movieDTOS.add(dto);
        }
        return movieDTOS;
    }

    @Override
    public MovieDTO getMovieDto(Movie movie) {
        return new MovieDTO(movie);
    }

    @Override
    public List<Movie> findAll() {
        return mr.findAll();
    }

    @Override
    public Movie findOne(long id) {
        return mr.findById(id).orElse(null);
    }

    @Override
    public Movie save(Movie movie) {
        return mr.save(movie);
    }

    @Override
    public void remove(long id) {
        mr.deleteById(id);
    }
}