package bobcat.cinema.service;

import bobcat.cinema.dto.GenreDTO;
import bobcat.cinema.model.Genre;
import bobcat.cinema.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GenreService implements GenreServiceInterface,GenreServiceDTOInterface{

    @Autowired
    GenreRepository gr;

    @Override
    public List<GenreDTO> getAllDTOs(List<Genre> genres) {
        List<GenreDTO> genreDTOS = new ArrayList<>();
        for (Genre genre : genres) {
            GenreDTO dto = new GenreDTO(genre);
            genreDTOS.add(dto);
        }
        return genreDTOS;
    }

    @Override
    public GenreDTO getGenreDto(Genre genre) {
        return new GenreDTO(genre);
    }

    @Override
    public List<Genre> findAll() {
        return gr.findAll();
    }

    @Override
    public Genre findOne(long id) {
        return gr.findById(id).orElse(null);
    }

    @Override
    public Genre save(Genre genre) {
        return gr.save(genre);
    }

    @Override
    public void remove(long id) {
        gr.deleteById(id);
    }
}