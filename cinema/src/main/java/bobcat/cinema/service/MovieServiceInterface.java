package bobcat.cinema.service;

import bobcat.cinema.model.Movie;

import java.util.List;

public interface MovieServiceInterface {

    List<Movie> findAll();

    Movie findOne(long id);

    Movie save(Movie movie);

    void remove(long id);
}