package bobcat.cinema.service;

import bobcat.cinema.dto.ProjectionTypeDTO;
import bobcat.cinema.model.ProjectionType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjecionTypeService implements ProjectionTypeServiceInterface, ProjectionTypeServiceDTOInterface {

    @Override
    public List<ProjectionTypeDTO> getAllDTOs(List<ProjectionType> projectionTypes) {
        return null;
    }

    @Override
    public ProjectionTypeDTO getProjectionTypeDto(ProjectionType projectionType) {
        return null;
    }

    @Override
    public List<ProjectionType> findAll() {
        return null;
    }

    @Override
    public ProjectionType findOne(long id) {
        return null;
    }

    @Override
    public ProjectionType save(ProjectionType projectionType) {
        return null;
    }

    @Override
    public void remove(long id) {

    }
}
