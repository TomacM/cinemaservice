package bobcat.cinema.service;

import bobcat.cinema.model.Movie;
import bobcat.cinema.model.ProjectionType;

import java.util.List;

public interface ProjectionTypeServiceInterface {

    List<ProjectionType> findAll();

    ProjectionType findOne(long id);

    ProjectionType save(ProjectionType projectionType);

    void remove(long id);
}
