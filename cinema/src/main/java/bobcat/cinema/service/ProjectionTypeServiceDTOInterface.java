package bobcat.cinema.service;

import bobcat.cinema.dto.MovieDTO;
import bobcat.cinema.dto.ProjectionTypeDTO;
import bobcat.cinema.model.Movie;
import bobcat.cinema.model.ProjectionType;

import java.util.List;

public interface ProjectionTypeServiceDTOInterface {

    List<ProjectionTypeDTO> getAllDTOs(List<ProjectionType> projectionTypes);

    ProjectionTypeDTO getProjectionTypeDto(ProjectionType projectionType);
}
