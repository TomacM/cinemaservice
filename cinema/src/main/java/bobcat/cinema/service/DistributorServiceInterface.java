package bobcat.cinema.service;

import bobcat.cinema.model.Distributor;

import java.util.List;

public interface DistributorServiceInterface {

    List<Distributor> findAll();

    Distributor findOne(long id);

    Distributor save(Distributor del);

    void remove(long id);
}