package bobcat.cinema.service;

import bobcat.cinema.dto.MovieDTO;
import bobcat.cinema.dto.PersonDTO;
import bobcat.cinema.model.Movie;
import bobcat.cinema.model.Person;

import java.util.List;

public interface PersonServiceDTOInterface {

    List<PersonDTO> getAllDTOs(List<Person> persons);

    PersonDTO getPeronDto(Person person);
}
