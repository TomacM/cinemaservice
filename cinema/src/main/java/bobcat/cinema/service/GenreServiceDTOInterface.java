package bobcat.cinema.service;

import bobcat.cinema.dto.GenreDTO;
import bobcat.cinema.dto.MovieDTO;
import bobcat.cinema.model.Genre;
import bobcat.cinema.model.Movie;

import java.util.List;

public interface GenreServiceDTOInterface {

    List<GenreDTO> getAllDTOs(List<Genre> genres);

    GenreDTO getGenreDto(Genre genre);
}
