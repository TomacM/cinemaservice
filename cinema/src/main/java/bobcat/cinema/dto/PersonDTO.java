package bobcat.cinema.dto;

import bobcat.cinema.model.Person;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonDTO {

    private Long id;
    private String firstName;
    private String lastname;

    public PersonDTO(Person obj) {
        id = obj.getId();
        firstName = obj.getFirstName();
        lastname = obj.getLastname();
    }
}