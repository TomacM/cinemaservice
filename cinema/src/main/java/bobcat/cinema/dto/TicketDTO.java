package bobcat.cinema.dto;

import bobcat.cinema.model.Ticket;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TicketDTO {

    private Long id;
    private ProjectionDTO projection;
    private SitDTO sit;
    private Date dateTimeOfReservation;
    private UserDTO user;
    private Long price;

    public TicketDTO(Ticket obj) {
        id = obj.getId();
        if (obj.getProjection() != null)
            projection = new ProjectionDTO(obj.getProjection());
        if (obj.getSit() != null)
            sit = new SitDTO(obj.getSit());
        dateTimeOfReservation = obj.getDateTimeOfReservation();
        if (obj.getUser() != null)
            user = new UserDTO(obj.getUser());
        price = obj.getPrice();
    }
}