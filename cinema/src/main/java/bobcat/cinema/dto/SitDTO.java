package bobcat.cinema.dto;

import bobcat.cinema.model.Sit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SitDTO {

    private Long id;
    private String row;
    private String sit;
    private TheaterDTO theater;
    private ProjectionDTO projection;

    public SitDTO(Sit obj) {
        id = obj.getId();
        row = obj.getRow();
        sit = obj.getSit();
        if (obj.getTheater() != null)
            theater = new TheaterDTO(obj.getTheater());
        if (obj.getProjection() != null)
            projection = new ProjectionDTO(obj.getProjection());
    }
}