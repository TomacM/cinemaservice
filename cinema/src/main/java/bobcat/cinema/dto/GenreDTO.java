package bobcat.cinema.dto;

import bobcat.cinema.model.Distributor;
import bobcat.cinema.model.Genre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GenreDTO {

    private Long id;
    private String name;

    public GenreDTO(Genre obj) {
        id = obj.getId();
        name = obj.getName();
    }
}