package bobcat.cinema.dto;

import bobcat.cinema.model.ProjectionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectionTypeDTO {

    private Long id;
    private String name;

    public ProjectionTypeDTO(ProjectionType obj) {
        id = obj.getId();
        name = obj.getName();
    }
}