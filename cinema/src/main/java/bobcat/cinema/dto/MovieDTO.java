package bobcat.cinema.dto;

import bobcat.cinema.model.Genre;
import bobcat.cinema.model.Movie;
import bobcat.cinema.model.Person;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MovieDTO {

    private Long id;
    private String name;
    private PersonDTO director;
    private Set<PersonDTO> actors;
    private Set<GenreDTO> genres;
    private String duration;
    private DistributorDTO distributor;
    private String country;
    private String year;
    private String description;

    public MovieDTO(Movie obj) {
        id = obj.getId();
        name = obj.getName();
        if (obj.getDirector() != null)
            director = new PersonDTO(obj.getDirector());
        duration = obj.getDuration();
        if (obj.getDistributor() != null)
            distributor = new DistributorDTO(obj.getDistributor());
        country = obj.getCountry();
        year = obj.getYear();
        description = obj.getDescription();
    }

    public void setActors(Set<Person> actors) {
        List<Person> rl = new ArrayList<Person>(actors);
        for (Person obj : rl)
            this.actors.add(new PersonDTO(obj));
    }

    public void setGenres(List<Genre> genres) {
        List<Genre> rl = new ArrayList<Genre>(genres);
        for (Genre obj : rl)
            this.genres.add(new GenreDTO(obj));
    }
}