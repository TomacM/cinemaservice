package bobcat.cinema.dto;

import bobcat.cinema.model.Projection;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectionDTO {

    private Long id;
    private MovieDTO movie;
    private ProjectionTypeDTO projectionType;
    private TheaterDTO theater;
    private Date dateTimeOfProjection;
    private String createdBy;

    public ProjectionDTO(Projection obj) {
        id = obj.getId();
        if (obj.getMovie() != null)
            movie = new MovieDTO(obj.getMovie());
        if (obj.getProjectionType() != null)
            projectionType = new ProjectionTypeDTO(obj.getProjectionType());
        if (obj.getTheater() != null)
            theater = new TheaterDTO(obj.getTheater());
        dateTimeOfProjection = obj.getDateTimeOfProjection();
        createdBy = obj.getCreatedBy();
    }
}