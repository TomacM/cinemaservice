package bobcat.cinema.dto;

import bobcat.cinema.model.ProjectionType;
import bobcat.cinema.model.Theater;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TheaterDTO {

    private Long id;
    private String name;
    private Set<ProjectionTypeDTO> projectionTypes;
    private Long numberRows;
    private Long numberSitsInRow;

    public TheaterDTO(Theater obj) {
        id = obj.getId();
        name = obj.getName();
        numberRows = obj.getNumberRows();
        numberSitsInRow = obj.getNumberSitsInRow();
    }

    public void setProjectionTypes(Set<ProjectionType> projectionTypes) {
        List<ProjectionType> rl = new ArrayList<ProjectionType>(projectionTypes);
        for (ProjectionType obj : rl)
            this.projectionTypes.add(new ProjectionTypeDTO(obj));
    }
}