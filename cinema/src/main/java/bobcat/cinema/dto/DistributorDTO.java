package bobcat.cinema.dto;

import bobcat.cinema.model.Distributor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DistributorDTO {

    private Long id;
    private String name;
    private String code;

    public DistributorDTO(Distributor obj) {
        id = obj.getId();
        name = obj.getName();
        code = obj.getCode();
    }
}