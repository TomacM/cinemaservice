package bobcat.cinema.dto;

import bobcat.cinema.model.User;
import bobcat.cinema.model.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private Long id;
    private String username;
    private String firstname;
    private String lastname;
    private String email;
    private String phone;
    private Date dayOfRegistration;
    private Role role;

    public UserDTO(User obj) {
        id = obj.getId();
        username = obj.getUsername();
        firstname = obj.getFirstname();
        lastname = obj.getLastname();
        email = obj.getEmail();
        phone = obj.getPhone();
        dayOfRegistration = obj.getDayOfRegistration();
        role = obj.getRole();

    }
}