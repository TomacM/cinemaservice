package bobcat.cinema.controller;

import bobcat.cinema.dto.DistributorDTO;
import bobcat.cinema.dto.GenreDTO;
import bobcat.cinema.model.Distributor;
import bobcat.cinema.model.Genre;
import bobcat.cinema.service.DistributorService;
import bobcat.cinema.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/genre")
public class GenreController {


    @Autowired
    GenreService gs;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<GenreDTO>> getAll() {
        List<Genre> genres = gs.findAll();
        List<GenreDTO> dtos = gs.getAllDTOs(genres);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<?> update(@RequestBody GenreDTO dto) {
        Genre genre = gs.findOne(dto.getId());
        if (genre == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        genre.setName(dto.getName());

        gs.save(genre);
        return new ResponseEntity<>(new GenreDTO(genre), HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<GenreDTO> getById(@PathVariable long id) {
        Genre gr = gs.findOne(id);
        if (gr == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        GenreDTO dto = gs.getGenreDto(gr);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> save(@RequestBody GenreDTO dto) {

        Genre genre = new Genre();
        genre.setName(dto.getName());

        gs.save(genre);
        return new ResponseEntity<>(new GenreDTO(genre), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable long id) {
        Genre genre = gs.findOne(id);
        if (genre != null) {
            gs.remove(id);
            List<Genre> genres = gs.findAll();
            List<GenreDTO> dtos = gs.getAllDTOs(genres);

            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

}