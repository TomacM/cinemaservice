package bobcat.cinema.controller;

import bobcat.cinema.dto.PersonDTO;
import bobcat.cinema.dto.ProjectionTypeDTO;
import bobcat.cinema.model.Person;
import bobcat.cinema.model.ProjectionType;
import bobcat.cinema.service.ProjecionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/types")
public class ProjectionTypeController {

    @Autowired
    ProjecionTypeService projecionTypeService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ProjectionTypeDTO>> getAll() {
        List<ProjectionType> projectionTypes = projecionTypeService.findAll();
        List<ProjectionTypeDTO> dtos = projecionTypeService.getAllDTOs(projectionTypes);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<?> update(@RequestBody ProjectionTypeDTO dto) {
        ProjectionType projectionType = projecionTypeService.findOne(dto.getId());
        if (projectionType == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        projectionType.setName(dto.getName());
        projecionTypeService.save(projectionType);
        return new ResponseEntity<>(new ProjectionTypeDTO(projectionType), HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProjectionTypeDTO> getById(@PathVariable long id) {
        ProjectionType projectionType = projecionTypeService.findOne(id);
        if (projectionType == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        ProjectionTypeDTO dto = projecionTypeService.getProjectionTypeDto(projectionType);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> save(@RequestBody ProjectionTypeDTO dto) {
        ProjectionType projectionType = new ProjectionType();
        projectionType.setName(dto.getName());
        projecionTypeService.save(projectionType);
        return new ResponseEntity<>(new ProjectionTypeDTO(projectionType), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable long id) {
        ProjectionType projectionType = projecionTypeService.findOne(id);
        if (projectionType != null) {
            projecionTypeService.remove(id);
            List<ProjectionType> projectionTypes = projecionTypeService.findAll();
            List<ProjectionTypeDTO> dtos = projecionTypeService.getAllDTOs(projectionTypes);

            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
