package bobcat.cinema.controller;


import bobcat.cinema.dto.DistributorDTO;
import bobcat.cinema.model.Distributor;
import bobcat.cinema.service.DistributorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/distributor")
public class DistributorController {

    @Autowired
    DistributorService ds;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<DistributorDTO>> getAll() {
        List<Distributor> distributors = ds.findAll();
        List<DistributorDTO> dtos = ds.getAllDTOs(distributors);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<?> update(@RequestBody DistributorDTO dto) {
        Distributor distributor = ds.findOne(dto.getId());
        if (distributor == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        distributor.setName(dto.getName());
        distributor.setCode(dto.getCode());

        ds.save(distributor);
        return new ResponseEntity<>(new DistributorDTO(distributor), HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<DistributorDTO> getById(@PathVariable long id) {
        Distributor dr = ds.findOne(id);

        if (dr == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        DistributorDTO dto = ds.getDistributorDTO(dr);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> save(@RequestBody DistributorDTO dto) {

        Distributor distributor = new Distributor();
        distributor.setName(dto.getName());
        distributor.setCode(dto.getCode());

        ds.save(distributor);
        return new ResponseEntity<>(new DistributorDTO(distributor), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable long id) {
        Distributor distributor = ds.findOne(id);
        if (distributor != null) {
            ds.remove(id);
            List<Distributor> distributors = ds.findAll();
            List<DistributorDTO> dtos = ds.getAllDTOs(distributors);

            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}