package bobcat.cinema.controller;

import bobcat.cinema.dto.PersonDTO;
import bobcat.cinema.model.Person;
import bobcat.cinema.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/person")
public class PersonController {

    @Autowired
    PersonService ps;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PersonDTO>> getAll() {
        List<Person> persons = ps.findAll();
        List<PersonDTO> dtos = ps.getAllDTOs(persons);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<?> update(@RequestBody PersonDTO dto) {
        Person person = ps.findOne(dto.getId());
        if (person == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        person.setFirstName(dto.getFirstName());
        person.setLastname(dto.getLastname());
        ps.save(person);
        return new ResponseEntity<>(new PersonDTO(person), HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<PersonDTO> getById(@PathVariable long id) {
        Person person = ps.findOne(id);
        if (person == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        PersonDTO dto = ps.getPeronDto(person);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<?> save(@RequestBody PersonDTO dto) {
        Person person = new Person();
        person.setFirstName(dto.getFirstName());
        person.setLastname(dto.getLastname());
        ps.save(person);
        return new ResponseEntity<>(new PersonDTO(person), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable long id) {
        Person person = ps.findOne(id);
        if (person != null) {
            ps.remove(id);
            List<Person> persons = ps.findAll();
            List<PersonDTO> dtos = ps.getAllDTOs(persons);

            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}